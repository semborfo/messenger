from PyQt5 import QtWidgets, QtCore
import client_ui
import datetime
import requests
import time

class Messenger(QtWidgets.QMainWindow, client_ui.Ui_Messenger):
    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.sendButton.pressed.connect(self.send_message)

        self.timer = QtCore.QTimer()
        self.timer.timeout.connect(self.update_messages)
        self.timer.start(1000)
        self.after = 0

    def update_messages(self):
        try:
            response = requests.get('http://127.0.0.1:5000/messages',
                                    params={'after': self.after})
        except:
            return
        for message in response.json()['messages']:
            dt = datetime.datetime.fromtimestamp(message['time'])
            dt = dt.strftime('%H:%M:%S')
            self.messageBrowser.append(f"{dt} {message['name']}:")
            self.messageBrowser.append(f"{message['text']}")
            self.add_empty_line()
            self.after = message['time']
        time.sleep(1)

    def add_empty_line(self):
        self.messageBrowser.append('')

    def send_message(self):
        name = self.nameInput.text()
        text = self.textInput.toPlainText()
        try:
            response = requests.post('http://127.0.0.1:5000/send',
                          json={'text': text, 'name': name})
        except:
            self.messageBrowser.append('Server is not available')
            self.add_empty_line()
            return
        if response.status_code == 400:
            self.messageBrowser.append('Check name and text')
            self.add_empty_line()
            return
        self.textInput.clear()



app = QtWidgets.QApplication([])
window = Messenger()
window.show()
app.exec_()
