from flask import Flask, request, abort
import time
from collections import Counter

app = Flask(__name__)

db = []


@app.route("/")
def hello():
    return "Hello, World! <a href='/status'>Status</a>"


@app.route("/status")
def status():
    return {
        'name': 'Messenger',
        'message_count': len(db),
        'users_online': len(Counter(msg['name'] for msg in db)),
        'time': time.time()
    }


@app.route("/messages")
def get_messages():
    try:
        after = float(request.args.get('after', 0))
    except:
        print('error')
        return abort(400)
    filtered_db = []
    max_len = 100
    for m in db:
        if m['time'] > after:
            filtered_db.append(m)
            max_len -= 1
        if not max_len:
            return {'messages': filtered_db}
    return {'messages': filtered_db}


@app.route("/send", methods=['POST'])
def send_message():
    if not isinstance(request.json, dict):
        return abort(400)
    text = request.json.get('text')
    name = request.json.get('name')
    if not text or not isinstance(text, str):
        return abort(400)
    if not name or not isinstance(name, str):
        return abort(400)

    message = {
        'text': text,
        'name': name,
        'time': time.time()
    }
    db.append(message)

    return {'ok': True}



app.run()
